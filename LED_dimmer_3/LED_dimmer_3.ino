#include "Wire.h"
#include "Adafruit_PWMServoDriver.h"

const int BUTTON_PIN = 2;
const int LONG_PRESS_THRESHOLD = 1000; // Long press threshold in milliseconds

Adafruit_PWMServoDriver PCA9685 = Adafruit_PWMServoDriver(0x41, Wire);

int brightness = 0; // Global variable to store LED brightness

void setup() {
  Serial.begin(9600);
  Serial.println("PCA9685LEDv3");

  pinMode(BUTTON_PIN, INPUT_PULLUP); // Set button pin as input with internal pull-up resistor
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonInterrupt, CHANGE); // Register button interrupt

  Wire.begin();
  PCA9685.begin();
  PCA9685.setPWMFreq(1600);  // This is the maximum PWM frequency and suited to LED's
}

void loop() {
  for (int q = 0; q < 16; q++) {
    PCA9685.setPWM(q, 0, brightness); // Set LED brightness to current value
  }
  delay(10); // Small delay to avoid overloading the I2C bus
}

void buttonInterrupt() {
  static unsigned long buttonPressTime = 0;
  if (digitalRead(BUTTON_PIN) == LOW) {
    // Button is pressed
    buttonPressTime = millis(); // Record button press time
  } else {
    // Button is released
    unsigned long buttonReleaseTime = millis(); // Record button release time
    if (buttonReleaseTime - buttonPressTime >= LONG_PRESS_THRESHOLD) {
      // Button was pressed for long enough, decrease brightness
      int newBrightness = max(0, brightness - 1000); // Decrease brightness by 1000 or until minimum value
      if (newBrightness != brightness) {
        brightness = newBrightness; // Update LED brightness if it has changed
        Serial.print("Brightness decreased to ");
        Serial.println(brightness);
      }
    }
  }
}
