//#include <ESP8266WiFi.h>
#include"HTTPSRedirect.h"
#include <WiFiClientSecure.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include"DHT.h"
#define DHTPIN D3 //connect DHT data pin to D3
#define DHTTYPE DHT11 // DHT 11
#define buzzer D4

DHT dht(DHTPIN, DHTTYPE);
// Fill ssid and password with your network credentials
const char* ssid ="Ngumb-H64";
const char* password ="DCollins@123";
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
const char* host ="script.google.com";
// Replace with your own script id to make server side changes
const char *GScriptId ="AKfycby4I436Q3BEgBXUEt3FChhTALz_x0N0ciR-Vir-0SBZVNg25dPRvzRj1MwJMa-4xY7W"; // Receiving data from google script address
const char *cellAddress1 ="A2"; //To set moisture value
const char *cellAddress2 ="B2"; // to set temperature value
String payload_base ="{\"command\": \"insert_row\", \"sheet_name\": \"Sheet1\", \"values\":";

const int httpsPort = 443;

String url1 = String("/macros/s/") + GScriptId +"/exec?read=" + cellAddress1;
String url2 = String("/macros/s/") + GScriptId +"/exec?read=" + cellAddress2;

String payload ="";
String payload1 ="";
String payload2 ="";
HTTPSRedirect* client = nullptr;
int msensor = A0; // moisture sensor is connected with the analog pin A1 of the Arduino
int msvalue = 0; // moisture sensor value
int mspercent; // moisture value in percentage
float temp; //to store the temperature value
float hum; // to store the humidity value

int Led = D0;
void setup() {

Serial.begin(115200);
Serial.flush();
Serial.println();
Serial.print("Connecting to wifi:");
Serial.println(ssid);
// flush() is needed to print the above (connecting…) message reliably,
// in case the wireless connection doesn’t go through
Serial.flush();

pinMode(msensor, INPUT);
pinMode(Led, OUTPUT);
digitalWrite(Led, LOW);

pinMode(DHTPIN, OUTPUT);
pinMode(buzzer, OUTPUT);
display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
delay(2000);
display.clearDisplay();
display.setTextColor(WHITE);
dht.begin(); //Begins to receive Temperature and humidity values.
WiFi.begin(ssid, password);
while (WiFi.status() != WL_CONNECTED) {
delay(500);
Serial.print(".");
}
Serial.println("");
Serial.println("WiFi connected");
Serial.println("IP address:");
Serial.println(WiFi.localIP());
}

void loop() {
temp = dht.readTemperature();

hum = dht.readHumidity();
Serial.print("temperature =");
Serial.println(temp);
Serial.print("humidity =");
Serial.println(hum);
msvalue = analogRead(msensor);
mspercent = map(msvalue,0,1023,100,0); // To display the soil moisture value in percentage
static int error_count = 0;
static int connect_count = 0;
const unsigned int MAX_CONNECT = 20;
static bool flag = false;
if (!flag){
client = new HTTPSRedirect(httpsPort);
client->setInsecure();
flag = true;
client->setPrintResponseBody(false);
client->setContentTypeHeader("application/json");
}

if (client != nullptr){
if (!client->connected()){
client->connect(host, httpsPort);
}
}
else{
Serial.println("Error creating client object!");
error_count = 5;
}

if (connect_count > MAX_CONNECT){
//error_count = 5;
connect_count = 0;
flag = false;
delete client;
return;
}

Serial.print("GET Data from cell 1 soil moisture:");
Serial.println(cellAddress1);
if (client->GET(url1, host)){
//get the value of the cell
payload1 = client->getResponseBody();
payload1.trim(); //soil moisture set value
Serial.println(payload1);
++connect_count;
}
Serial.print("GET Data from cell 2:");
Serial.println(cellAddress2);
if (client->GET(url2, host)){
//get the value of the cell
payload2 = client->getResponseBody();
payload2.trim(); /// temperature value
Serial.println(payload2);
++connect_count;
}

if (error_count > 3){
Serial.println("Halting processor…");
delete client;
client = nullptr;
Serial.flush();
ESP.deepSleep(0);
}

// Add some delay in between checks
delay(1000);
int moisture = payload1.toInt();
int ctemp= payload2.toInt();

if(mspercent<=moisture)
{
digitalWrite(buzzer,HIGH);
}

if(mspercent>moisture)
{
digitalWrite(buzzer,LOW);
}

if(temp>=ctemp)
{
digitalWrite(Led,LOW);
}

if(temp<ctemp)
{
digitalWrite(Led,HIGH);
}

display.clearDisplay();
display.setCursor(10,0);
display.setTextSize(2);
display.setTextColor(WHITE);
display.print("SM:"+String(moisture)+"%");
display.setCursor(10,30);
display.setTextSize(2);
display.print("ST:"+String(ctemp));

display.display();
sendData(temp, hum, mspercent); //–> Calls the sendData Subroutine
}
// Subroutine for sending data to Google Sheets
void sendData(float value0, int value1, int value2) {

String GAS_ID ="AKfycbzvzLN75zU3n_Y__nvmhsw9ycvmuQYOnSzmEoepDDnFeIYrdztJ-wjvmvpN17Tth0oD"; //sending data to google script address

String url = String("/macros/s/") + GAS_ID+"/exec";

payload = payload_base +"\"" + value0 +"," + value1 +"," + value2 +"\"}";

// Publish data to Google Sheets
Serial.println("Publishing data…");
Serial.println(payload);
if(client->POST(url, host, payload)){
// do stuff here if publish was successful
}
else{
// do stuff here if publish was not successful
Serial.println("  Error while connecting");
}

// a delay of several seconds is required before publishing again
delay(5000);
//—————————————-Processing data and sending data

}
