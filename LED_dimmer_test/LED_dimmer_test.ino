#include "Wire.h"
#include "Adafruit_PWMServoDriver.h"


struct Button {
   const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
  uint32_t lastPressTime;
};


Button button1 = {18, 0, false};



Adafruit_PWMServoDriver PCA9685 = Adafruit_PWMServoDriver(0x40, Wire);
 int btn = 1;
 int btn2 = 0;
 int lim;
 int q;



///button interrupt service routine
void IRAM_ATTR isr() {
  if (digitalRead(button1.PIN) == LOW) {
    button1.lastPressTime = millis();
    button1.pressed = true;
  } else {
    button1.pressed = false;
  }
}




void setup() {
  Serial.begin(9600);
  pinMode(button1.PIN, INPUT_PULLUP);

  attachInterrupt(button1.PIN, isr, CHANGE);


  Wire.begin();
  PCA9685.begin();
  PCA9685.setPWMFreq(1600);  // This is the maximum PWM frequency and suited to LED's

  for(q=0;q<16;q++){
          Serial.print("pwm:");
          Serial.println(lim);
          PCA9685.setPWM(q, 0, 0);
      }
}

void loop(){

  if (button1.pressed) {
    if (millis() - button1.lastPressTime >= 1000) { // long press duration is 1 second (1000 ms)
      Serial.println("Long pressed");
      for(q=0;q<16;q++){
          Serial.print("pwm:");
          Serial.println(lim);
          PCA9685.setPWM(q, 0, lim);
      }
      
        lim  = lim + 50;
        Serial.print("pwm:");
        Serial.println(lim);

        if(lim >4096 ){
          lim = 0;
          Serial.print("Resetting pwm....");
        }
        delay(3);
    }
  } else {
    button1.lastPressTime = millis();
  }




     

}
