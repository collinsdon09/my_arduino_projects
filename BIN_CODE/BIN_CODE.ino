  // Blinks an LED attached to a MCP23XXX pin.

// ok to include only the one needed
// both included here to make things simple for example
#include <Adafruit_MCP23X08.h>
#include <Adafruit_MCP23X17.h>

#define PIN_0 0    
#define PIN_1 1    
#define PIN_2 2    
#define PIN_3 3    
#define PIN_4 4    
#define PIN_5 5    
#define PIN_6 6    
#define PIN_7 7
#define PIN_8 8
#define PIN_9 9 
#define PIN_10 10   
#define PIN_11 11  
#define PIN_12 12   
#define PIN_13 13   
#define PIN_14 14   
#define PIN_15 15   
#define PIN_16 16   


 

  
   
    
// only used for SPI
#define CS_PIN 6

// uncomment appropriate line
//Adafruit_MCP23X08 mcp;
Adafruit_MCP23X17 mcp;

void setup() {
  Serial.begin(9600);
  //while (!Serial);
  Serial.println("MCP23xxx Blink Test!");

  // uncomment appropriate mcp.begin
  if (!mcp.begin_I2C()) {
  //if (!mcp.begin_SPI(CS_PIN)) {
    Serial.println("Error.");
    while (1);
  }

  // configure pin for output or Input
  mcp.pinMode(PIN_0, OUTPUT);
  mcp.pinMode(PIN_1, OUTPUT);
  mcp.pinMode(PIN_2, OUTPUT);
  mcp.pinMode(PIN_3, OUTPUT);
  mcp.pinMode(PIN_4, OUTPUT);
  mcp.pinMode(PIN_5, OUTPUT);
  mcp.pinMode(PIN_6, OUTPUT);
  mcp.pinMode(PIN_7, OUTPUT);
  mcp.pinMode(PIN_8, OUTPUT);
  mcp.pinMode(PIN_9, OUTPUT);
  mcp.pinMode(PIN_10, OUTPUT);
  mcp.pinMode(PIN_11, OUTPUT);
  mcp.pinMode(PIN_12, OUTPUT);
  mcp.pinMode(PIN_13, OUTPUT);
  mcp.pinMode(PIN_14, OUTPUT);
  mcp.pinMode(PIN_15, OUTPUT);
  mcp.pinMode(PIN_16, OUTPUT);



 




  Serial.println("Looping...");
}

void loop() {
  mcp.digitalWrite(PIN_0, HIGH);
  delay(500);
  mcp.digitalWrite(PIN_0, LOW);
  delay(500);
}
