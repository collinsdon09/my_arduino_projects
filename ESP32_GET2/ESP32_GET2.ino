
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>

const char* ssid = "Ngumb-H64";
const char* password = "DCollins@123";

//Your Domain name with URL path or IP address with path
const char* serverName = "http://192.168.100.19:8000/status";


unsigned long lastTime = 0;

unsigned long timerDelay = 50;

String server_response;
float sensorReadingsArr[3];

void setup() {
  pinMode(2, OUTPUT);
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
}

void loop() {
  //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
              
      server_response = httpGETRequest(serverName);
      Serial.print("sensor readings:");
      Serial.println(sizeof(server_response.toInt()));

      JSONVar myObject = JSON.parse(server_response);
  
      // JSON.typeof(jsonVar) can be used to get the type of the var
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject[0]);
      String obj = myObject[0];
      Serial.print("obj from server:");
      Serial.println(obj);

      if(obj == "1"){
        Serial.println("ON");
        digitalWrite(2, HIGH);
      }else{
           digitalWrite(2, LOW);
          Serial.println("OFF");
      }

            
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}






String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}





//
//
// //UPDATE STATUS
//void update_status() {
// 
// if(WiFi.status()== WL_CONNECTED){
// 
//   HTTPClient http;   
// 
//   http.begin("http://192.168.100.19:8000/update-status/"+esp32);
//   http.addHeader("Content-Type", "text/plain");            
// 
//   int httpResponseCode = http.PUT("PUT sent from ESP32");   
// 
//   if(httpResponseCode>0){
// 
//    String response = http.getString();   
// 
//    Serial.println(httpResponseCode);
//    Serial.println(response);          
// 
//   }else{
// 
//    Serial.print("Error on sending PUT Request: ");
//    Serial.println(httpResponseCode);
// 
//   }
// 
//   http.end();
// 
// }else{
//    Serial.println("Error in WiFi connection");
// }
// 
//  delay(1000);
//}
