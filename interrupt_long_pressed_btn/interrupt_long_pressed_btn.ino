struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
  uint32_t lastPressTime;
};

Button button1 = {16, 0, false, 0};

void IRAM_ATTR isr() {
  if (digitalRead(button1.PIN) == LOW) {
    button1.lastPressTime = millis();
    button1.pressed = true;
  } else {
    button1.pressed = false;
  }
}

void setup() {
  Serial.begin(9600);
  pinMode(button1.PIN, INPUT_PULLUP);
  attachInterrupt(button1.PIN, isr, CHANGE);
}

void loop() {
  if (button1.pressed) {
    if (millis() - button1.lastPressTime >= 1000) { // long press duration is 1 second (1000 ms)
      Serial.println("Long pressed");
      delay(500); // add a delay to avoid multiple prints when the button is released
    }
  } else {
    button1.lastPressTime = millis();
  }
}
