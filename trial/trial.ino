#include <SPI.h>
#include <Ethernet.h>

// Enter your server's IP address here
IPAddress server(192, 168, 1, 100);

EthernetClient client;

void setup() {
  Serial.begin(9600);

  // Initialize the Ethernet shield
  if (Ethernet.begin() == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    while(true);
  }
}

void loop() {
  // Make a HTTP GET request to the server
  if (client.connect(server, 80)) {
    Serial.println("Connected to server");
    client.println("GET /index.html HTTP/1.1");
    client.println("Host: 192.168.1.100");
    client.println("Connection: close");
    client.println();
  }
  else {
    Serial.println("Failed to connect to server");
  }

  // Wait for the server to respond
  while (client.connected()) {
    if (client.available()) {
      char c = client.read();
      Serial.print(c);
    }
  }

  // Disconnect from the server
  client.stop();

  // Wait for 5 seconds before making another request
  delay(5000);
}
