
#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>
#include <DHT.h>
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager



#define DHTPIN 4 
#define DHTTYPE DHT22   


const char* ssid = "Ngumb-H64";
const char* password = "DCollins@123";

//Your Domain name with URL path or IP address with path
const char* serverName = "http://192.168.100.19:8000/status";


unsigned long lastTime = 0;
unsigned long timerDelay = 50;

String server_response;
float sensorReadingsArr[3];
String server_post ="http://192.168.100.19:8000/sensor_post_3";

String humidity;
String temperature;

DHT dht(DHTPIN, DHTTYPE);








void setup() {
  pinMode(2, OUTPUT);
  Serial.begin(115200);
  dht.begin();
  wifi_manager();

//
//  WiFi.begin(ssid, password);
//  Serial.println("Connecting");
//  while(WiFi.status() != WL_CONNECTED) {
//    delay(500);
//    Serial.print(".");
//  }
//  Serial.println("");
//  Serial.print("Connected to WiFi network with IP Address: ");
//  Serial.println(WiFi.localIP());
 
}


void loop(){
//  functionality();
//  update_status();
    post_request();
  
}









void get_request() {
  //Send an HTTP POST request every 10 minutes
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
              
      server_response = httpGETRequest(serverName);
      Serial.print("sensor readings:");
      Serial.println(sizeof(server_response.toInt()));

      JSONVar myObject = JSON.parse(server_response);
  
      // JSON.typeof(jsonVar) can be used to get the type of the var
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject[0]);
      String obj = myObject[0];
      Serial.print("obj from server:");
      Serial.println(obj);

      if(obj == "1"){
        Serial.println("ON");
        digitalWrite(2, HIGH);
      }else{
           digitalWrite(2, LOW);
          Serial.println("OFF");
      }

            
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}











void functionality() {

  
  }








String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, serverName);
  
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}





void post_request() {
  //Send an HTTP POST request every 10 minutes
  delay(1000);

    float h = dht.readHumidity();
    float t = dht.readTemperature();

//
//   if (isnan(h) || isnan(t)) {
//          Serial.println(F("Failed to read from DHT sensor!"));
//    return;
//  }

//
//  humidity = String(h);
//  temperature = String(t);


  humidity = "70";
  temperature = "20";
  

  

  
//
//  Serial.print(F("Humidity: "));
//  Serial.print(humidity + "%");
//  Serial.print(F("%  Temperature: "));
//  Serial.print(temperature +"°C");
//  Serial.print(F("°C "));










  
  if ((millis() - lastTime) > timerDelay) {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;
    
      // Your Domain name with URL path or IP address with path
      http.begin(client, server_post);
      

      http.addHeader("Content-Type", "application/json");
        String Data = "{\"Humidity\":\"" + humidity +
                     "\",\"Temperature\":\"" + temperature + 
                       "\"}";

      int httpResponseCode = http.POST(Data);


      // If you need an HTTP request with a content type: text/plain
      //http.addHeader("Content-Type", "text/plain");
      //int httpResponseCode = http.POST("Hello, World!");
     
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
        
      // Free resources
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}




void wifi_manager() {
    // WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
    // it is a good practice to make sure your code sets wifi mode how you want it.

    // put your setup code here, to run once:
//    Serial.begin(115200);
    
    //WiFiManager, Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wm;

    // reset settings - wipe stored credentials for testing
    // these are stored by the esp library
     wm.resetSettings();

    // Automatically connect using saved credentials,
    // if connection fails, it starts an access point with the specified name ( "AutoConnectAP"),
    // if empty will auto generate SSID, if password is blank it will be anonymous AP (wm.autoConnect())
    // then goes into a blocking loop awaiting configuration and will return success result

    bool res;
    // res = wm.autoConnect(); // auto generated AP name from chipid
    // res = wm.autoConnect("AutoConnectAP"); // anonymous ap
    res = wm.autoConnect("AutoConnectAP","password"); // password protected ap

    if(!res) {
        Serial.println("Failed to connect");
        // ESP.restart();
    } 
    else {
        //if you get here you have connected to the WiFi    
        Serial.println("connected...yeey :)");
    }

}
