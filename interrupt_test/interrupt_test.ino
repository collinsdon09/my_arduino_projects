struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};


Button button1 = {18, 0, false};
Button button2 = {16, 0, false};


void IRAM_ATTR isr() {
  button1.numberKeyPresses++;
  button1.pressed = true;
}



void IRAM_ATTR isr2() {
  button2.numberKeyPresses++;
  button2.pressed = true;

}







void setup() {
  Serial.begin(115200);
  pinMode(button1.PIN, INPUT_PULLUP);
  pinMode(button2.PIN, INPUT_PULLUP);

  attachInterrupt(button1.PIN, isr, FALLING);
  attachInterrupt(button2.PIN, isr2, FALLING);

}

void loop() {
  if (button1.pressed) {
    Serial.printf("Button has been pressed %u times\n", button1.numberKeyPresses);
    button1.pressed = false;
  }


  if (button2.pressed) {
    Serial.printf("Button 2 has been pressed %u times\n", button2.numberKeyPresses);
    button2.pressed = false;
  }


}
