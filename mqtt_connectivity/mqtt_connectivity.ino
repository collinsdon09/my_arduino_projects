#include <MQTTPubSubClient.h>

#include <SPI.h> //protocol to communicate to the ethernet module
#include <Ethernet.h> //library to run webclient / web server over ethernet
#include <ArduinoMqttClient.h>

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; //physical mac address set to the ethernet module
byte ip[] = { 192, 168, 0, 12 }; // IP address in LAN – need to change according to your Network address
byte gateway[] = { 192, 168, 0, 1 }; // gateway IP address
byte subnet[] = { 255, 255, 255, 0 }; //subnet mask
EthernetClient ethclient; // create a client object
MqttClient mqttClient(ethclient);
const char broker[] = "test.mosquitto.org";
int        port     = 1883;
const char topic[]  = "arduino/simple";

const long interval = 1000;
unsigned long previousMillis = 0;

int count = 0;





void setup() {
  //start Ethernet
  Ethernet.begin(mac, ip, gateway, subnet); //initialize ethernet
  Serial.begin(9600); //initialize serial communication
  delay(1000); //give the Ethernet shield time to initialize

    Serial.println("You're connected to the network");
  Serial.println();

 
  Serial.print("Attempting to connect to the MQTT broker: ");
  Serial.println(broker);

  if (!mqttClient.connect(broker, port)) {
    Serial.print("MQTT connection failed! Error code = ");
    Serial.println(mqttClient.connectError());

    while (1);
  }

  Serial.println("You're connected to the MQTT broker!");
  Serial.println();

}

void loop() {
  mqtt_loop();
//  if (client.connect("example.com", 80)) { // make a TCP connection to the API server
//    client.println("GET /api/endpoint HTTP/1.1"); // send the GET request to the API endpoint
//    client.println("Host: example.com");
//    client.println("Connection: close");
//    client.println();
//  } else {
//    Serial.println("Connection failed"); // if connection fails, print error message to serial monitor
//  }
//
//  while (client.connected()) { // wait for server response
//    if (client.available()) {
//      char c = client.read(); // read the response character by character
//      Serial.print(c); // print the response to serial monitor
//    }
//  }
//
//  client.stop(); // close the connection
//  delay(5000); // wait for 5 seconds before making the next request
}




void mqtt_loop() {
  // call poll() regularly to allow the library to send MQTT keep alives which
  // avoids being disconnected by the broker
  mqttClient.poll();

  // to avoid having delays in loop, we'll use the strategy from BlinkWithoutDelay
  // see: File -> Examples -> 02.Digital -> BlinkWithoutDelay for more info
  unsigned long currentMillis = millis();
  
  if (currentMillis - previousMillis >= interval) {
    // save the last time a message was sent
    previousMillis = currentMillis;

    Serial.print("Sending message to topic: ");
    Serial.println(topic);
    Serial.print("hello ");
    Serial.println(count);

    // send message, the Print interface can be used to set the message contents
    mqttClient.beginMessage(topic);
    mqttClient.print("hello ");
    mqttClient.print(count);
    mqttClient.endMessage();

    Serial.println();

    count++;
  }
}
