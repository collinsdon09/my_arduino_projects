from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel
import json





status_variable=""






class Sensor_status(BaseModel):
    led: str
    relay: str
    pressure_sensor: str



class Sensor_status_2(BaseModel):
    pressure_sensor_1: str
    pressure_sensor_2: str
    pressure_sensor_3: str
    pressure_sensor_4: str
   
    led_1: str
    led_2: str
    led_3: str
    led_4: str

    relay_1: str
    relay_2: str
    relay_3: str
    relay_4: str


class Sensor_status_3(BaseModel):
    Humidity: str
    Temperature: str
 


class Sensor_status_smoke_quality(BaseModel):
    Smoke_Status: str
    Buzzer_Status: str



class Sensor_status_motion_sensor(BaseModel):
    motion_sensor: str
    led: str
    buzzer:str



class Sensor_status_reed_sensor(BaseModel):
    reed_switch: str
    led: str

class Sensor_status_lux_sensor(BaseModel):
    light_status: str
    led: str
 
 


 
 
    
 
 
    





app = FastAPI()


@app.get("/")
def read_root():
    # return {"Hello": "World"}
    return {
    "postId": 2,
    "id": 10,
    "name": "eaque et deleniti atque tenetur ut quo ut",
    "email": "Carmen_Keeling@caroline.name",
    "body": "voluptate iusto quis nobis reprehenderit ipsum amet nulla\nquia quas dolores velit et non\naut quia necessitatibus\nnostrum quaerat nulla et accusamus nisi facilis"
        
        
    }









    #New GET request






@app.get('/status')
def get_status():
    # status = status_model.status
    file = open('demofile2.txt', 'r')
    list_1 = list(file.read())
        

    return (list_1 + ["2"])



@app.get('/change-status')
def change_status():
    status_variable="True"
    file = open('demofile2.txt', 'r+')
    file.write(status_variable)





#REAL PROJECT FROM HERE

@app.put('/update-status/{status}')
def update_status(status):
    
    print(type(status))
    state = status
    file = open('demofile2.txt', 'r+')

    file.truncate(0)
    file.write(status)

    return status




@app.put('/update-status-project-1/{status}')
def update_status(status):
    
    print(type(status))
    state = status
    file = open('project_1.txt', 'r+')

    file.truncate(0)
    file.write(status)

    return status



#PROJECT 2--PRESSURE SENSOR DATA
@app.post('/sensor_post')
def create_sensor(esp32_incoming_json: Sensor_status):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    led = esp32_incoming_json.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json)
    json_string = json.dumps(dict(esp32_incoming_json))
    file_json = open('pressure_sensor_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json}'}



@app.get('/sensor-info')
def get_sensor_info():
    file = open('pressure_sensor_data.json', 'r+')
    data = json.load(file)
    return data 



#PROJECT 2
@app.post('/sensor_post_2')
def create_sensor(esp32_incoming_json_2: Sensor_status_2):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json_2)
    json_string = json.dumps(dict(esp32_incoming_json_2))
    file_json = open('multi_switch_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_2}'}


@app.get('/sensor-info_2')
def get_sensor_info():
    file = open('multi_switch_data.json', 'r+')
    data = json.load(file)
    return data
    




#PROJECT 3
@app.post('/sensor_post_3')
def create_sensor(esp32_incoming_json_3: Sensor_status_3):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json_3)
    json_string = json.dumps(dict(esp32_incoming_json_3))
    json_string(json_string )

    file_json = open('temperature_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_3}'}


@app.get('/sensor-info_3')
def get_sensor_info():
    file = open('temperature_data.json', 'r+')
    data = json.load(file)
    return data 








#PROJECT 4- SMOKE SENSOR
@app.post('/sensor_post_4')
def create_sensor(esp32_incoming_json_4: Sensor_status_smoke_quality):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json_4)
    json_string = json.dumps(dict(esp32_incoming_json_4))

    file_json = open('air_quality_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_4}'}


@app.get('/sensor-info_4')
def get_sensor_info():
    file = open('air_quality_data.json', 'r+')
    data = json.load(file)
    return data 





#PROJECT 5- MOTION SENSOR
@app.post('/sensor_post_5')
def create_sensor(esp32_incoming_json_5: Sensor_status_motion_sensor):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json_5)
    json_string = json.dumps(dict(esp32_incoming_json_5))

    file_json = open('motion_sensor_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_5}'}


@app.get('/sensor-info_5')
def get_sensor_info():
    file = open('motion_sensor_data.json', 'r+')
    data = json.load(file)
    return data 




#PROJECT 6- REED SWITCH
@app.post('/sensor_post_6')
def create_sensor(esp32_incoming_json_6: Sensor_status_reed_sensor):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing individual element
    print("JSON SENT::",esp32_incoming_json_6)
    json_string = json.dumps(dict(esp32_incoming_json_6))

    file_json = open('magnetic_switch_data.json', 'r+')
    file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_6}'}


@app.get('/sensor-info_6')
def get_sensor_info():
    file = open('magnetic_switch_data.json', 'r+')
    data = json.load(file)
    return data 




#PROJECT 7- LUX SENSOR
@app.post('/sensor_post_7')
def create_sensor(esp32_incoming_json_7: Sensor_status_lux_sensor):   #def create_sensor(esp32_incoming_json: type):   
    # username = user.username
    # led = esp32_incoming_json_2.led #accesing value for a individual key in the Json
    print("JSON SENT::",esp32_incoming_json_7)
    json_string = json.dumps(dict(esp32_incoming_json_7))

    # file_json = open('magnetic_switch_data.json', 'r+')
    # file_json.write(json_string)
    return {'message:' f'successfully created user:{esp32_incoming_json_7}'}


@app.get('/sensor-info_7')
def get_sensor_info():
    file = open('magnetic_switch_data.json', 'r+')
    data = json.load(file)
    return data 







