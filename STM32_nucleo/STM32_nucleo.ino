#include <Wire.h>
//            SDA  SCL
TwoWire Wire1(PB6, PB7);

void setup() {
  Wire1.begin(); 
}

void loop() {
  Wire1.beginTransmission(0x71);
  Wire1.write('v');
  Wire1.endTransmission();
  delay(1000);
}
