#include <WiFi.h>
#include <PubSubClient.h>

// Replace with your network credentials
const char* ssid = "Ngumb-H64";
const char* password = "DCollins@123";

// Replace with your Mosquitto broker IP address or domain name
const char* mqtt_server = "https://test.mosquitto.org";

// Replace with your topic name
const char* topic = "arduino/simple";

WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(9600);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }

  Serial.println("Connected to WiFi");

  client.setServer(mqtt_server, 1883);

  while (!client.connected()) {
    Serial.println("Connecting to Mosquitto broker...");

    if (client.connect("arduino_client")) {
      Serial.println("Connected to Mosquitto broker");
    } else {
      Serial.print("Failed to connect to Mosquitto broker, rc=");
      Serial.print(client.state());
      Serial.println(" retrying in 5 seconds");

      delay(5000);
    }
  }
}

void loop() {
  client.loop();

  if (!client.connected()) {
    reconnect();
  }

  client.publish(topic, "hello");
  delay(5000);
}

void reconnect() {
  while (!client.connected()) {
    Serial.println("Reconnecting to Mosquitto broker...");

    if (client.connect("arduino_client")) {
      Serial.println("Connected to Mosquitto broker");
    } else {
      Serial.print("Failed to connect to Mosquitto broker, rc=");
      Serial.print(client.state());
      Serial.println(" retrying in 5 seconds");

      delay(5000);
    }
  }
}
