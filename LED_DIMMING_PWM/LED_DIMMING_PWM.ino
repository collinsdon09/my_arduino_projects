#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// Create an instance of the Adafruit_PWMServoDriver class
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

// Define the pins for the button and the LED
int buttonPin = 2;   // the pin number of the button
int ledPin = 0;      // the channel number of the LED

// Define the minimum and maximum values for the PWM signal
int pwmMin = 0;
int pwmMax = 4095;

// Define the LED brightness and the button state variables
int ledBrightness = pwmMin;
int buttonState = LOW;

void setup() {
  // Initialize the PWM controller
  pwm.begin();
  pwm.setPWMFreq(1000);  // Set the PWM frequency to 1000 Hz

  // Set the initial brightness of the LED
  pwm.setPWM(ledPin, 0, ledBrightness);

  // Set the button pin as input and enable the internal pull-up resistor
  pinMode(buttonPin, INPUT_PULLUP);
}

void loop() {
  // Read the state of the button
  buttonState = digitalRead(buttonPin);

  // If the button is pressed and held down for more than 1 second,
  // increase the LED brightness by a fixed increment
  if (buttonState == LOW) {
    delay(1000);  // Wait for 1 second
    while (digitalRead(buttonPin) == LOW) {
      ledBrightness += 100;  // Increment the LED brightness by 100
      if (ledBrightness > pwmMax) ledBrightness = pwmMin;
      pwm.setPWM(ledPin, 0, ledBrightness); // Set the LED brightness
      delay(10);  // Wait for the LED to reach the new brightness
    }
  }
}
