#include "Wire.h"
#include "Adafruit_PWMServoDriver.h"

struct Button {
   const uint8_t PIN;
   uint32_t numberKeyPresses;
   bool pressed;
   uint32_t lastPressTime;
};

Button button1 = {18, 0, false};
Button button2 = {16, 0, false};

Adafruit_PWMServoDriver PCA9685 = Adafruit_PWMServoDriver(0x40, Wire);

int lim = 0;

void IRAM_ATTR isr1() {
  if (digitalRead(button1.PIN) == LOW) {
    button1.lastPressTime = millis();
    button1.pressed = true;
  } else {
    button1.pressed = false;
  }
}

void IRAM_ATTR isr2() {
  if (digitalRead(button2.PIN) == LOW) {
    button2.lastPressTime = millis();
    button2.pressed = true;
  } else {
    button2.pressed = false;
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(button1.PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button1.PIN), isr1, CHANGE);

  pinMode(button2.PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button2.PIN), isr2, CHANGE);

  Wire.begin();
  PCA9685.begin();
  PCA9685.setPWMFreq(1600);

  for (int i = 0; i < 16; i++) {
    PCA9685.setPWM(i, 0, 0);
  }
}

void loop() {
  if (button1.pressed) {
    if (millis() - button1.lastPressTime >= 1000) {
      Serial.println("Button 1 long press");

      lim += 50;
      if (lim > 4096) {
        lim = 4096;
      }

      for (int i = 0; i < 16; i++) {
        PCA9685.setPWM(i, 0, lim);
      }

      Serial.print("PWM value: ");
      Serial.println(lim);
    }
  } else {
    button1.lastPressTime = millis();
  }

  if (button2.pressed) {
    if (millis() - button2.lastPressTime >= 1000) {
      Serial.println("Button 2 long press");

      lim -= 50;
      if (lim < 0) {
        lim = 0;
      }

      for (int i = 0; i < 16; i++) {
        PCA9685.setPWM(i, 0, lim);
      }

      Serial.print("PWM value: ");
      Serial.println(lim);
    }
  } else {
    button2.lastPressTime = millis();
  }
}
