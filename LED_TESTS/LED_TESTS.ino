#include <Wire.h>
//#include <Adafruit_MCP23017.h>
#include <Adafruit_MCP23X17.h>


// Create two instances of the MCP23017 class
Adafruit_MCP23X17 mcp1;
Adafruit_MCP23X17 mcp2;

void setup() {
  // Initialize the I2C communication
  Wire.begin();
 
  // Initialize the two MCP23017
  mcp1.begin_I2C(0x20);
  mcp2.begin_I2C(0x27);

  // Set all pins of MCP1 as outputs
  mcp1.pinMode(0, OUTPUT);
  mcp1.pinMode(1, OUTPUT);
  mcp1.pinMode(2, OUTPUT);
  // ...
 
  // Set all pins of MCP2 as inputs
  mcp2.pinMode(0, OUTPUT);
  mcp2.pinMode(1, OUTPUT);
  mcp2.pinMode(2, OUTPUT);
  // ...
}

void loop() {
  // Read the state of an input pin on MCP2
  int state = mcp2.digitalRead(0);

  // Write a value to an output pin on MCP1
  mcp1.digitalWrite(0, HIGH);
  mcp2.digitalWrite(0, HIGH);

}
