#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// Create an instance of the Adafruit_PWMServoDriver class
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

// Define the pins for the buttons and the servo motor
int buttonPin1 = 2;  // the pin number of button 1
int buttonPin2 = 3;  // the pin number of button 2
int servoPin = 0;    // the channel number of the servo motor

// Define the minimum and maximum values for the servo motor position and PWM signal
int servoMin = 150;
int servoMax = 600;
int pwmMin = 150;
int pwmMax = 600;

// Define the servo position and the button states variables
int servoPos = servoMin;
int buttonState1 = LOW;
int buttonState2 = LOW;

void setup() {
  // Initialize the PWM servo motor controller
  pwm.begin();
  pwm.setPWMFreq(50);  // Set the PWM frequency to 50 Hz

  // Set the initial position and PWM signal of the servo motor
  pwm.setPWM(servoPin, 0, servoMin);

  // Set the button pins as input and enable the internal pull-up resistors
  pinMode(buttonPin1, INPUT_PULLUP);
  pinMode(buttonPin2, INPUT_PULLUP);
}

void loop() {
  // Read the state of the buttons
  buttonState1 = digitalRead(buttonPin1);
  buttonState2 = digitalRead(buttonPin2);

  // If button 1 is pressed and held down for more than 1 second,
  // rotate the servo motor by a fixed increment
  if (buttonState1 == LOW) {
    delay(1000);  // Wait for 1 second
    while (digitalRead(buttonPin1) == LOW) {
      servoPos += 10;  // Increment the servo position by 10
      if (servoPos > servoMax) servoPos = servoMin;
      pwm.setPWM(servoPin, 0, servoPos); // Set the servo position
      delay(15);  // Wait for the servo to reach the new position
    }
  }

  // If button 2 is pressed and held down for more than 1 second,
  // decrease the PWM signal of the servo motor by a fixed increment
  if (buttonState2 == LOW) {
    delay(1000);  // Wait for 1 second
    while (digitalRead(buttonPin2) == LOW) {
      servoPos -= 10;  // Decrement the servo position by 10
      if (servoPos < servoMin) servoPos = servoMin;
      int pwmVal = map(servoPos, servoMin, servoMax, pwmMin, pwmMax);
      pwm.setPWM(servoPin, 0, pwmVal); // Set the PWM signal
      delay(15);  // Wait for the servo to reach the new position
    }
  }
}
